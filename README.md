dotfiles
========

Symlink the appropriate dotfiles:
> ln -s ~/.dotfiles/.vimrc ~/.vimrc

> ln -s ~/.dotfiles/.zshrc ~/.zshrc